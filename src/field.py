import re
from itertools import product, starmap

def can_be_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

class ImmutableError(Exception):
    pass

class NANError(Exception):
    pass

class Field(object):
    BOMB = 'X'
    EMPTY = '/'
    UNKNOWN = '·'

    BOMBS_ALLOWED = 10

    def __init__(self):
        self.field = []
        self.numbers = []
        self.bombs = []
        self.width = 0
        self.height = 0

        self.current = 0

    def set(self, x, y, val):
        if val not in [Field.BOMB, Field.EMPTY, Field.UNKNOWN]:
            raise TypeError
        self.field[y][x] = val

    def unset(self, x, y):
        if self.get(x, y) == Field.BOMB:
            self.bombs.remove(self.xy_to_i(x, y))
        self.set(x, y, Field.UNKNOWN)

    def get(self, x, y):
        return self.field[y][x]

    def get_num(self, x, y):
        if(self.is_num(x, y)):
            return int(self.get(x, y))
        return NANError

    def i_to_xy(self, i):
        return i % self.height, i // self.height

    def xy_to_i(self, x, y):
        return y * self.width + x

    def is_num(self, x, y):
        return can_be_int(self.get(x, y))

    def is_bomb(self, x, y):
        return self.get(x, y) == Field.BOMB

    def is_empty(self, x, y):
        return self.get(x, y) == Field.EMPTY

    def is_unknown(self, x, y):
        return self.get(x, y) == Field.UNKNOWN

    def set_bomb(self, x, y):
        if self.is_num(x, y):
            raise ImmutableError
        self.unset(x, y)
        self.bombs.append(self.xy_to_i(x, y))
        self.set(x, y, Field.BOMB)

    def set_empty(self, x, y):
        if self.is_num(x, y):
            raise ImmutableError
        self.unset(x, y)
        self.set(x, y, Field.EMPTY)

    def set_unknown(self, x, y):
        if self.is_num(x, y):
            raise ImmutableError
        self.unset(x, y)
        self.set(x, y, Field.UNKNOWN)

    def get_numbers(self):
        numbers = []
        for x in range(self.width):
            for y in range(self.height):
                v = self.get(x, y)
                if can_be_int(v):
                    numbers.append((int(v), (x, y)))
        return numbers

    def load(self, mine_file):
        self.field = []
        with open(mine_file) as f:
            for line in f:
                self.field.append([Field.UNKNOWN if x == '-' else x for x in line.rstrip()])

        self.width = len(self.field) if len(self.field) > 0 else 0
        self.height = len(self.field)
        self.numbers = self.get_numbers()
        return self.field

    def is_in_grid(self, x, y):
        return x >= 0 and x < self.width and y >= 0 and y < self.height

    def get_adj(self, x, y):
        return list(filter(lambda i: self.is_in_grid(i[0], i[1]) and (i[0] != x or i[1] != y),
                starmap(lambda a,b: (x+a, y+b), product((0, -1, +1), (0, -1, +1)))))

    def num_adj_bombs(self, x, y):
        return sum([1 for i in self.get_adj(x, y) if self.is_bomb(i[0], i[1])])

    def is_sat(self, x, y):
        return self.num_adj_bombs(x, y) == self.get_num(x, y)

    def get_unsats(self):
        return [n for n in self.numbers if not self.is_sat(n[1][0], n[1][1])]

    def bombs_remaining(self):
        return Field.BOMBS_ALLOWED - len(self.bombs)

    def __iter__(self):
        self.current = 0
        return self

    def __next__(self):
        if self.current >= len(self):
            raise StopIteration
        else:
            x,y = self.i_to_xy(self.current)
            self.current += 1
            return self.get(x, y)

    def __len__(self):
        return self.height * self.width
