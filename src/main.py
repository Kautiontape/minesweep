#!/usr/bin/env python

import sys
from field import Field, ImmutableError

change_stack = []

def print_field(field):
    print('    {}'.format(' '.join(map(str, list(range(field.width))))))
    print('   ' + '-'*(field.width*2))
    for y in range(field.height):
        print('{} | {} | {}'.format(y, ' '.join(field.field[y]), y))
    print('   ' + '-'*(field.width*2))
    print('    {}'.format(' '.join(map(str, list(range(field.width))))))
    print("")
    print("{} Unsatisfied".format(len(field.get_unsats())))
    print("{} Bombs Remaining".format(field.bombs_remaining()))

def ask_action():
    while True:
        c = input("What would you like to do ([P]lace / [U]ndo / [S]tatus / [R]eset)? ")[0].upper()
        if c in ['P', 'U', 'S', 'R']:
            return c

        print("Not a valid action")

def ask_mark():
    while True:
        c = input("What would you like to place ([B]omb / [E]mpty / [?])? ")[0].upper()
        if c in ['B', 'E', '?']:
            return {'B' : Field.BOMB, 'E' : Field.EMPTY, '?' : Field.UNKNOWN}[c]
        print("Not a valid type")

def ask_pos(maxx, maxy):
    while True:
        pos = list(map(int, input("Enter an x and y position to place the mark (X Y): ").split(' ')))
        if len(pos) < 2:
            print("Invalid input")
        elif pos[0] >= 0 and pos[0] <= maxx and pos[1] >= 0 and pos[1] <= maxy:
            return pos[0], pos[1]
        else:
            print("Position ({}, {}) is not in the grid".format(pos[0], pos[1]))

def set_mark(field, x, y, mark):
    if mark == Field.BOMB:
        field.set_bomb(x, y)
    elif mark == Field.UNKNOWN:
        field.set_unknown(x, y)
    elif mark == Field.EMPTY:
        field.set_empty(x, y)

def action_place(field):
    mark = ask_mark()
    x, y = ask_pos(field.width - 1, field.height - 1)
    try:
        v = field.get(x, y)
        set_mark(field, x, y, mark)
        change_stack.append((x, y, v))
    except ImmutableError:
        print("Can not place in that position")

def action_undo(field):
    if len(change_stack) > 0:
        c = change_stack.pop()
        set_mark(field, c[0], c[1], c[2])
    else:
        print("No actions to undo")

def action_reset(field):
    while len(change_stack) > 0:
        action_undo(field)

def action_status(field):
    unsats = field.get_unsats()

    print("")
    print("You have {} bombs remaining to place".format(field.bombs_remaining()))
    print("You have {} unsatisfied numbers:".format(len(unsats)))
    for u in unsats:
        x, y = u[1][0], u[1][1]
        b = field.num_adj_bombs(x, y)
        print("    {} at ({}, {}) has {} bombs".format(u[0], x, y, b), end="")
        print(" ({} remaining)".format(u[0] - b))

def main(mine_file):
    field = Field()
    field.load(mine_file)

    while len(field.get_unsats()) > 0 or field.bombs_remaining() < 0:
        print('\n  //// THE FIELD \\\\\\\\')
        print_field(field)
        action = ask_action()

        if action == 'P':
            action_place(field)
        elif action == 'U':
            action_undo(field)
        elif action == 'S':
            action_status(field)
        elif action == 'R':
            action_reset(field)

    print("---- SOLUTION ----")
    print_field(field)
    return


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("No minesweeper file specified")
